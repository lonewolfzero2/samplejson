package com.trustudio.android.samplejson.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.trustudio.android.samplejson.R;
import com.trustudio.android.samplejson.model.Contact;

import java.util.ArrayList;

/**
 * Created by Default on 8/18/2015.
 */
public class ContactAdapter extends BaseAdapter{

    private LayoutInflater mInflater;
    private ArrayList<Contact> arrlist;
    private static Context context;

    public ContactAdapter(Context c) {
        mInflater = LayoutInflater.from(c);
        context = c;
    }

    public void setArray(ArrayList<Contact> dList) {
        this.arrlist = dList;
    }

    @Override
    public int getCount() {
        return arrlist.size();
    }

    @Override
    public Object getItem(int position) {
        return arrlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_list_contact, null);
            holder = new ViewHolder();

            holder.name         = (TextView) convertView.findViewById(R.id.name);
            holder.username     = (TextView)convertView.findViewById(R.id.email);


            notifyDataSetChanged();

            convertView.setTag(holder);

        } else {

            holder = (ViewHolder) convertView.getTag();
        }
        Contact data = arrlist.get(position);

        holder.name.setText(data.name);
        holder.username.setText(data.email);

        notifyDataSetChanged();

        return convertView;
    }

    class ViewHolder {
        TextView name,username;

    }
}

